import React, {Component} from 'react';

export default (props) => (
  <div className="wrapper">{props.children}</div>
)