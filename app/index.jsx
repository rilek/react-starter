import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import Routes from './routes';

import Layout from './containers/Layout/Layout';
import {} from './views/common/common.scss';
import store from './store';


ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route component={Routes} />
    </BrowserRouter>
  </Provider>,
  document.getElementById('app')
);